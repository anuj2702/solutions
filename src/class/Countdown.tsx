import React from "react";
import { createStyles, withStyles, WithStyles } from "@material-ui/styles";
import { Theme } from "@material-ui/core";

const styles = (theme: Theme) =>
  createStyles({
    background: {
      backgroundColor: "#eee",
      height: "97vh",
      width: "100%",
    },
    timer: {
      width: "70%",
      height: "70%",
      margin: "0 auto",
      paddingTop: "5%",
    },
    header: {
      backgroundColor: "#343434",
      color: "white",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      padding: "8px 12px",
      borderRadius: "4px",
    },
    button: {
      margin: "0 8px",
      fontSize: "16px",
      padding: "12px 16px",
      backgroundColor: "#02fecd",
      borderRadius: "4px",
      "&:hover": {
        cursor: "pointer",
      },
    },
    title: { fontSize: "20px" },
    countdown: {
      backgroundColor: "#090909",
      height: "80%",
      paddingTop: "10%",
    },
    boxes: {
      display: "flex",
      justifyContent: "center",
    },
    box: {
      height: "160px",
      width: "160px",
      color: "white",
      backgroundColor: "#343434",
      alignItems: "center",
      margin: "20px",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
    },
    endsIn: {
      color: "white",
      fontSize: "28px",
      marginBottom: "20px",
      display: "flex",
      justifyContent: "center",
    },
    time: {
      fontSize: "48px",
      color: "#02fecd",
    },
    unit: {
      fontSize: "20px",
      color: "white",
    },
  });

interface Props extends WithStyles<typeof styles> {
  timeInSeconds: number;
}

interface State {
  time: number;
}

class Countdown extends React.Component<Props, State> {
  timerId: NodeJS.Timeout | null;

  constructor(props: Props) {
    super(props);
    this.state = { time: props.timeInSeconds };
    this.timerId = null;
    this.startTimer = this.startTimer.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.handlePause = this.handlePause.bind(this);
  }

  startTimer = () => {
    this.timerId = setInterval(
      () => this.setState((s) => ({ time: s.time - 1 })),
      1000
    );
  };

  componentDidMount() {
    this.startTimer();
  }

  componentWillUnmount() {
    if (this.timerId) {
      clearInterval(this.timerId);
    }
  }

  handleClear = () => this.setState({ time: this.props.timeInSeconds });

  handlePause = () => {
    if (this.timerId) {
      clearInterval(this.timerId);
      this.timerId = null;
    } else {
      this.startTimer();
    }
    this.forceUpdate();
  };

  render() {
    const {
      props: { classes },
      state: { time },
      timerId,
      handleClear,
      handlePause,
    } = this;

    const days = Math.floor(time / 86400);
    const remainingHours = time % 86400;
    const hours = Math.floor(remainingHours / 3600);
    const remainingMinutes = remainingHours % 3600;
    const minutes = Math.floor(remainingMinutes / 60);
    const seconds = remainingMinutes % 60;

    const showDays = days > 0;
    const showHours = days > 0 || hours > 0;
    const showMinutes = days > 0 || hours > 0 || minutes > 0;
    const showSeconds = days > 0 || hours > 0 || minutes > 0 || seconds > 0;

    return (
      <div className={classes.background}>
        <div className={classes.timer}>
          <div className={classes.header}>
            <span className={classes.title}>Countdown Timer</span>
            <div>
              <button className={classes.button} onClick={handlePause}>
                {timerId ? <>Pause</> : <>Resume</>}
              </button>

              <button className={classes.button} onClick={handleClear}>
                Clear
              </button>
            </div>
          </div>
          <div className={classes.countdown}>
            <div className={classes.endsIn}>Countdown ends in...</div>
            <div className={classes.boxes}>
              {showDays && (
                <div className={classes.box}>
                  <div className={classes.time}>{days}</div>{" "}
                  <div className={classes.unit}>Days</div>
                </div>
              )}
              {showHours && (
                <div className={classes.box}>
                  <div className={classes.time}>{hours}</div>{" "}
                  <div className={classes.unit}>Hours</div>
                </div>
              )}
              {showMinutes && (
                <div className={classes.box}>
                  <div className={classes.time}>{minutes}</div>{" "}
                  <div className={classes.unit}>Mins</div>
                </div>
              )}
              {showSeconds && (
                <div className={classes.box}>
                  <div className={classes.time}>{seconds}</div>{" "}
                  <div className={classes.unit}>Secs</div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Countdown);
