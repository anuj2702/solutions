import React from "react";
import { Theme } from "@material-ui/core";
import { createStyles, WithStyles, withStyles } from "@material-ui/styles";

interface Props extends WithStyles<typeof styles> {}

interface State {
  time: number;
}

const styles = (theme: Theme) =>
  createStyles({
    background: {
      backgroundColor: "#eee",
      height: "97vh",
      width: "100%",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    stopwatch: {
      width: "320px",
      height: "320px",
      backgroundColor: "#104d92",
      borderRadius: "8px",
      paddingTop: "8px",
    },
    header: {
      color: "white",
      display: "flex",
      justifyContent: "center",
      padding: "8px 12px",
      borderRadius: "4px",
    },
    title: { fontSize: "20px" },
    card: {
      backgroundColor: "white",
      height: "76%",
      margin: "12px -40px",
      borderRadius: "4px",
      boxShadow: "0px 2px 8px #aaa",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
    },
    time: {
      padding: "60px 0",
      fontSize: "28px",
    },
    button: {
      margin: "0 8px",
      fontSize: "16px",
      padding: "8px 20px",
      backgroundColor: "white",
      border: "2px solid #8081ff",
      borderRadius: "12px",
      color: "#8081ff",
      "&:hover": {
        cursor: "pointer",
      },
    },
  });

class Stopwatch extends React.Component<Props, State> {
  timerId: NodeJS.Timeout | null;

  constructor(props: Props) {
    super(props);
    this.state = { time: 0 };
    this.timerId = null;
    this.start = this.start.bind(this);
    this.pause = this.pause.bind(this);
    this.handleReset = this.handleReset.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  start = () => {
    this.timerId = setInterval(
      () => this.setState((s) => ({ time: s.time + 1 })),
      1000
    );
  };
  pause = () => {
    clearInterval(this.timerId!);
    this.timerId = null;
  };

  handleClick = () => {
    if (this.timerId) {
      this.pause();
    } else {
      this.start();
    }
    this.forceUpdate();
  };

  handleReset = () => {
    this.pause();
    this.setState((s) => ({ time: 0 }));
  };

  render() {
    const {
      props: { classes },
      state: { time },
      timerId,
      handleClick,
      handleReset,
    } = this;

    const hours = Math.floor(time / 3600);
    const remainingMinutes = time % 3600;
    const minutes = Math.floor(remainingMinutes / 60);
    const seconds = remainingMinutes % 60;

    return (
      <div className={classes.background}>
        <div className={classes.stopwatch}>
          <div className={classes.header}>
            <span className={classes.title}>React Stopwatch</span>
          </div>
          <div className={classes.card}>
            <div className={classes.time}>
              {hours < 10 && "0"}
              {hours} : {minutes < 10 && "0"}
              {minutes} : {seconds < 10 && "0"}
              {seconds}
            </div>

            <div>
              <button className={classes.button} onClick={handleClick}>
                {timerId ? <>Pause</> : <>Start</>}
              </button>
              <button className={classes.button} onClick={handleReset}>
                Reset
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Stopwatch);
