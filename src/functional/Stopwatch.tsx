import React from "react";
import { makeStyles } from "@material-ui/styles";
import useForceUpdate from "use-force-update";

interface Props {}

const useStyles = makeStyles(() => ({
  background: {
    backgroundColor: "#eee",
    height: "97vh",
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  stopwatch: {
    width: "320px",
    height: "320px",
    backgroundColor: "#104d92",
    borderRadius: "8px",
    paddingTop: "8px",
  },
  header: {
    color: "white",
    display: "flex",
    justifyContent: "center",
    padding: "8px 12px",
    borderRadius: "4px",
  },
  title: { fontSize: "20px" },
  card: {
    backgroundColor: "white",
    height: "76%",
    margin: "12px -40px",
    borderRadius: "4px",
    boxShadow: "0px 2px 8px #aaa",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  time: {
    padding: "60px 0",
    fontSize: "28px",
  },
  button: {
    margin: "0 8px",
    fontSize: "16px",
    padding: "8px 20px",
    backgroundColor: "white",
    border: "2px solid #8081ff",
    borderRadius: "12px",
    color: "#8081ff",
    "&:hover": {
      cursor: "pointer",
    },
  },
}));

const Stopwatch: React.FC<Props> = ({ ...props }) => {
  const classes = useStyles();
  const [time, setTime] = React.useState(0);
  const ref = React.useRef<NodeJS.Timeout | null>(null);
  const forceUpdate = useForceUpdate();

  const start = () => {
    ref.current = setInterval(() => setTime((t) => t + 1), 1000);
  };
  const pause = () => {
    clearInterval(ref.current!);
    ref.current = null;
  };

  const handleClick = () => {
    if (ref.current) {
      pause();
    } else {
      start();
    }
    forceUpdate();
  };

  const handleReset = () => {
    pause();
    setTime(0);
  };

  const hours = Math.floor(time / 3600);
  const remainingMinutes = time % 3600;
  const minutes = Math.floor(remainingMinutes / 60);
  const seconds = remainingMinutes % 60;

  return (
    <div className={classes.background}>
      <div className={classes.stopwatch}>
        <div className={classes.header}>
          <span className={classes.title}>React Stopwatch</span>
        </div>
        <div className={classes.card}>
          <div className={classes.time}>
            {hours < 10 && "0"}
            {hours} : {minutes < 10 && "0"}
            {minutes} : {seconds < 10 && "0"}
            {seconds}
          </div>

          <div>
            <button className={classes.button} onClick={handleClick}>
              {ref.current ? <>Pause</> : <>Start</>}
            </button>
            <button className={classes.button} onClick={handleReset}>
              Reset
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Stopwatch;
