import React from "react";
import FCT from "../functional/Countdown";
import CCT from "../class/Countdown";
import Wrapper from "./Wrapper";
import FSW from "../functional/Stopwatch";
import CSW from "../class/Stopwatch";

export default {
  title: "Solutions",
  decorators: [(storyFn: () => JSX.Element) => <Wrapper>{storyFn()}</Wrapper>],
};

export const FunctionalCountdownTimer = () => <FCT timeInSeconds={8975} />;
export const ClassCountdownTimer = () => <CCT timeInSeconds={128975} />;
export const FunctionalStopwatch = () => <FSW />;
export const ClassStopwatch = () => <CSW />;
